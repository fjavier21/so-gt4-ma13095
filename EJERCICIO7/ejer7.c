#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main()
{
    int tamanio;
    do
    {
        printf("Ingrese el grado del polinomio:");
        scanf("%d", &tamanio);
    } while (tamanio <= 0);
    tamanio++;
    double *coeficientes = (double *)malloc(tamanio * (sizeof(double)));

    printf("Ingrese los coeficientes en el orden de izquierda a derecha.\n");
    for (int i = 0; i < tamanio; i++)
    {
        printf("Coeficiente #%d: ", i + 1);
        scanf("%lf", coeficientes);
        coeficientes++;
    }
    double x;
    printf("Ingrese el dato con el que desea evaluar el polinomio:");
    scanf("%lf", &x);
    coeficientes--;
    double sumatoria;
    sumatoria = sumatoria + *coeficientes;
    coeficientes--;
    for (double j = 1; j < tamanio; j++)
    {
        sumatoria = sumatoria + (*coeficientes * pow(x, j));
        coeficientes--;
    }
    coeficientes++;
    tamanio--;
    printf("P(x)=");
    for (int j = 0; j < tamanio; j++)
    {
        printf("%lf(X^%d)+", *coeficientes, x, (tamanio - j));
        coeficientes++;
    }
    printf("%lf\n", *coeficientes);
    printf("P(%lf)=%lf\n", x, sumatoria);
}