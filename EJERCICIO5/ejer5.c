#include <stdio.h>
#include <stdlib.h>

int main()
{
    int *numeros = (int *)malloc(100 * sizeof(int));
    for (int i = 1; i <= 100; i++)
    {
        *numeros = rand() % 100;
        if (i % 10 == 0)
        {
            printf("%d\n", *numeros);
        }
        else
        {
            printf("%d--", *numeros);
        }
        numeros++;
    }
}