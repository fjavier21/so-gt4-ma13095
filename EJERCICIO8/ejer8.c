#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *pedirTexto();
void contarVocales(char *, int[]);
void imprimir(int[]);

void main()
{
    char *texto;
    int num[5];
    texto = pedirTexto();
    contarVocales(texto, num);
    imprimir(num);
}

char *pedirTexto()
{
    char *texto = (char *)malloc(100 * sizeof(char));
    printf("ingrese el texto:");
    scanf("%[^\n]", texto);
    return texto;
}

void contarVocales(char *puntero, int datos[])
{
    int a = 0;
    int e = 0;
    int i = 0;
    int o = 0;
    int u = 0;

    for (int j = 0; j < strlen(puntero); j++)
    {
        if (puntero[j] == 'a' || puntero[j] == 'A')
        {
            a = a + 1;
        }
        else if (puntero[j] == 'e' || puntero[j] == 'E')
        {
            e = e + 1;
        }
        else if (puntero[j] == 'i' || puntero[j] == 'I')
        {
            i = i + 1;
        }
        else if (puntero[j] == 'o' || puntero[j] == 'O')
        {
            o = o + 1;
        }
        else if (puntero[j] == 'u' || puntero[j] == 'U')
        {
            u = u + 1;
        }
    }
    datos[0] = a;
    datos[1] = e;
    datos[2] = i;
    datos[3] = o;
    datos[4] = u;
}

void imprimir(int vocales[])
{
    printf("Vocales en el texto:\n");
    printf("a=%d\n", vocales[0]);
    printf("e=%d\n", vocales[1]);
    printf("i=%d\n", vocales[2]);
    printf("o=%d\n", vocales[3]);
    printf("u=%d\n", vocales[4]);
}